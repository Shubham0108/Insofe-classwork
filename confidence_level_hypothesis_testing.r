#############################################################
# Description : Labwork on Central limit, Confidence Interval and Hypothesis
#               testing dated 25th August.
# Author : Shubham Maheshwari
##############################################################



########################### Question 1 ########################
s.1 = sqrt(0.81) / sqrt(35) #Standard error
m.1 = 2.364
ci.1 = c(m.1 + s.1*qnorm(0.05), m.1 + s.1*qnorm(0.95))
print(paste("Considering 90% confidence the Critical Interval is in range",ci.1[1],"-",ci.1[2]))
##############################################################


########################### Question 2 ########################
s.2 = 1.5 / sqrt(40)
m.2 = 22
claimed.mean = 25
ci.2 = c(m.2 + s.2*qnorm(0.025), m.2 + s.2*qnorm(0.975))
if (ci.2[1] < claimed.mean && ci.2[2] > claimed.mean) {
  print("Manufracturer's claim is correct")
} else {
  print("Manufracturer's claim is false")
}
##############################################################


########################### Question 3 ########################
m.3 = 2.6
s.3 = 0.3 / sqrt(36)
############ 95% Confidence ############
ci.3.95 = c(m.3 + s.3*qnorm(0.05), m.3 + s.3*qnorm(0.95))
print(paste("Considering 90% confidence the Critical Interval is in range",ci.3.95[1],"-",ci.3.95[2]))
############ 99% Confidence ############
ci.3.99 = c(m.3 + s.3*qnorm(0.025), m.3 + s.3*qnorm(0.975))
print(paste("Considering 99% confidence the Critical Interval is in range",ci.3.99[1],"-",ci.3.99[2]))
###############################################################


########################### Question 4 ########################
############ Question 4.a #############
# Null Hypothesis H0 : Mean snowfall = 21.8
# Alternate Hypothesis H1 : Mean snowfall != 21.8.
# Critical Range : x < 0.05 and x > 0.95
############ Question 4.b #############
# Null Hypothesis H0 : Number of faculty contributing <=20%
# Alternate Hypothesis H1 : Number of faculty contributing >20%
# Critical Range : x > 0.05
###############################################################


########################### Question 5 ########################
s.5 = 2.5 / sqrt(35)
m.5 = 14.6
previous.mean = 15.4
ci.5 = c(m.5 + s.5*qnorm(0.025), m.5 + s.5*qnorm(0.975))
if (ci.5[1] < previous.mean && previous.mean < ci.5[2]) {
  print("Null hypothesis accepted")
} else {
  print("Null hypothesis rejected")
}
###############################################################


########################### Question 6 ########################
########## Using Binomial Distribution ###################
1 - pbinom(20,50,0.5) # Probability with 2 options
1 - pbinom(20,50,0.25) # Probability with 4 options
########## Using Normal Distribution ###################
m.6.1 = 50 * 0.5
s.6.1 = sqrt(50*0.5*0.5)
1-pnorm(20,m.6.1,s.6.1) # Probability with 2 options
m.6.2 = 50 * 0.25
s.6.2 = sqrt(50*0.25*0.75)
1-pnorm(20,m.6.2,s.6.2) # Probability with 4 options
###############################################################


########################### Question 7 ########################
values = c(68,42,51,57,56,80,45,39,36,79)
m.7 = mean(values)
s.7 = sd(values) / sqrt(10)
ci.7 = c(m.7 + s.7*qt(0.025,9), m.7 + s.7*qt(0.975,9))
print(paste("Considering 95% confidence the Critical Interval is in range",ci.7[1],"-",ci.7[2]))
###############################################################