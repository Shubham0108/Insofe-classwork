#######################################################################
rm(list=ls(all=TRUE))
setwd("~/Code/Insofe-classwork/R/Logistic and Naive Bayes")
#######################################################################

#######################################################################
library(caret)
library(e1071)
library(infotheo)
#######################################################################

#######################################################################
flight.data = read.csv("Flight_Delays.csv")


sum(train$Flight.Status == 1) / nrow(train)
sum(test$Flight.Status == 1) / nrow(test)

summary(flight.data)

flight.data$Weather = as.factor(flight.data$Weather)
flight.data$DAY_OF_MONTH = as.factor(flight.data$DAY_OF_MONTH)
flight.data$DAY_WEEK = as.factor(flight.data$DAY_WEEK)
flight.data$Flight.Status = as.factor(flight.data$Flight.Status)
flight.data$FL_NUM = as.factor(flight.data$FL_NUM)
str(flight.data)

time.transform = function(flight.data){
  delay = data.frame(matrix(ncol = 1,nrow = 0))
  names(delay) = "Delay"
  delay.times = flight.data$DEP_TIME - flight.data$CRS_DEP_TIME
  for( time in delay.times){
    if(time > 0){
      row = "Delayed"
    } else if (time == 0){
      row = "On-Time"
    } else {
      row = "Before-time"
    }
    delay[nrow(delay)+1,] = row
  }
  return(delay)
}

delay = time.transform(flight.data)

flight.data = cbind(flight.data,delay)
sum(flight.data$Delay=="Delayed")
flight.data$CRS_DEP_TIME = NULL
flight.data$DEP_TIME = NULL

flight.data$DISTANCE = as.factor(as.character(flight.data$DISTANCE))
flight.data$Delay = as.factor(flight.data$Delay)
str(flight.data)

split.index = createDataPartition(flight.data$Flight.Status, list = F, p = 0.7)
train = flight.data[split.index,]
test = flight.data[-split.index,]

sum(train$Delay=="Delayed")

model = naiveBayes(Flight.Status~., data = train)
pred.train = predict(model, train, type="class")
pred.test = predict(model, test, type = "class")

table(train$Flight.Status,pred.train)
table(test$Flight.Status,pred.test)

train.metrics = confusionMatrix(pred.train,train$Flight.Status)
train.metrics$overall["Kappa"]
test.metrics = confusionMatrix(pred.test,test$Flight.Status)
test.metrics$overall["Kappa"]
