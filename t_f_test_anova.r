#############################################################
# Description : Labwork on Chi Square, F test and ANOVA testing 
#               dated 26th August.
# Author : Shubham Maheshwari
##############################################################


########################### Question 1 ########################
# Null hypothesis : Gender does not impact choosing of hobbies
# Alternative hypothesis : Gender impacts choosing of hobbies
observed.1 = matrix(c(100,120,60,
                     350,200,90), byrow = TRUE, ncol = 3)
test.stat.1 = chisq.test(observed.1)
test.stat.1$p.value > 0.05 
# Since the above condition is false we can reject the Null Hypothesis
# Therefore we conclude that Gender does have an impact on choosing hobbies
###############################################################


########################### Question 2 ########################
# Null hypothesis : Company's claim is correct
# Alternate hypothesis : Company's claim is false
obsevred.frequencies.2 = c(50,45,5)
expected.proportions.2 = c(0.3,0.6,0.1)
chiSquare.calculated.2 = chisq.test(x=obsevred.frequencies.2, 
                                   p = expected.proportions.2)
chiSquare.calculated.2$p.value > 0.05
# Since the above condition is False we can reject the Null hypothesis
# Therefore, we can conclude that company's claim is false
###############################################################


########################### Question 3 ########################
# Null hypotheis : People do not prefer any particular coffee
# Alternate hypothesis : People prefer a particular coffee
observed.3 = matrix(c(3,5,6,2,1,2,6,7,9,7,11,6,9,10,15,12,11,10),
                    ncol =3)
test.stat.3 = chisq.test(observed.3)
test.stat.3$p.value > 0.05 
# Sicne the above condition return true we fail to reject the hypothesis
# Therefore, we conclude that people do not have preferecnce for any
# particular coffee.
###############################################################


########################### Question 4 ########################
# Null hypothesis : There is no descrepency in the system
# Alternate hypothesis : There is a decscrepency in the system
obsevred.frequencies.4 = c(402, 358, 273, 467)
expected.frequencies.4 = rep(375,4)
expected.proportions.4 = expected.frequencies.4 / sum(expected.frequencies.4)
chiSquare.calculated.4 = chisq.test(x=obsevred.frequencies.4, 
                                   p = expected.proportions.4)
chiSquare.calculated.4$p.value > 0.05
# Since the above condition is False we can reject the Null hypothesis
# Therefore, we conclude that there is a decscrepency in the system
###############################################################


########################### Question 5 ########################
# Null hypotheis : mean1 = mean2 = mean3 , mean price is same in all years
# Alternate hypothesis : Atleast one mean price is not than other groups 
x.5.1 = c(30000,34000,36000,38000,40000)
x.5.2 = c(30000,35000,37000,38000,40000)
x.5.3 = c(40000,41000,43000,44000,50000)
x.5 = c(x.5.1,x.5.2,x.5.3)
data.5 = data.frame(scores = x.5, groups = factor(rep(c(1996,1997,1998),c(5,5,5))))
model.5 = aov(scores~groups, data = data.5)
summary(model.5)
############## 95% Confidence #################
# Since the p-value is 0.0104 which is less than 0.05 we can reject the 
# Null hypothesis.
# Therefore, we conlcude that at 95 % confidence the prices across 
# the years are not same.
############## 99% Confidence #################
# Here the p-value 0.0104 is more than the critical p-value 0.01.
# Therefore we fail to reject the Null hypothesis.
# Therefore, we conclude that at 99% confidence the prices across 
# the years is same.
###############################################################


########################### Question 6 ########################
# Null hypothesis : Mileage and no of cylnders are independent
# Alternate hypothesis : Milage and no of cylinders are not independent
mpg = mtcars$mpg
cyl = mtcars$cyl
model.6 = aov(mpg~cyl,data = mtcars[1:2])
summary(model.6)
# Since the p(f value) is 6.11e-10 which is less than 0.05
# we can reject the Null hypothesis
# Therefore, we conclude that milage is significantly affected by no of 
# cylinders
###############################################################