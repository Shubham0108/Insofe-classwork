# Clear the environment variables
rm(lsit=ls())

# Set the working directory
setwd("~/Code/Insofe-classwork/Linear Regression")

# Import the libraries
library(DMwR)
library(caret)

# Read the data
df = read.csv('cars.csv', header = T, sep = ',')

# Impute missing values
factor(df$Reliability, ordered = T)
df = centralImputation(df)

# Create train and test data
set.seed(3)
train.validation.index = createDataPartition(df$Country, list = FALSE, p=0.7)
train = df[train.validation.index,]
validation = df[-train.validation.index,]

# Create Result data frame
results.data.frame = data.frame(matrix(ncol = 3, nrow = 0))
names(results.data.frame) = c("col_name","rmse_train","rmse_validate")
col_names = names(train[,-1])

# Loop for each column
for(col in col_names){
    linear.formula = as.formula(paste("Price ~ ",col))
    linear.regression.model = lm(linear.formula, data = train)
    train_pred = predict(object = linear.regression.model, newdata = train)
    train_rmse = regr.eval(trues = train$Price, preds = train_pred)[3]
    validate.pred = predict(linear.regression.model,newdata = validation)
    validate_rmse = regr.eval(trues = validation$Price, validate.pred)[3]
    row = c(col, as.numeric(train_rmse), as.numeric(validate_rmse))
    results.data.frame[nrow(results.data.frame) + 1,] = row
}
results.data.frame
