######################################################################
# Description : Classwork on probability basics dated 18 August, 2018
# Author : Shubham Maheshwari
######################################################################



############################# Question 1 #############################
total = 100
################### 1.a ####################
count.male.and.graduate = 19
probability.male.and.graduate =  count.male.and.graduate / total
print(paste("Probability that a randomly selected individual is a male and a graduate is",
            probability.male.and.graduate))
# Kind of probability is Joint
################### 1.b ####################
count.male = 60
probability.male = count.male / total
print(paste("Probability that a randomly selected individual is a male is",
            probability.male))
# Kind of probability is Marginal
################### 1.c ####################
count.graduate = 31
probability.graduate = count.graduate / total
print(paste("Probability that a randomly selected individual is a graduate is",
            probability.graduate))
# Kind of probability is Marginal
################### 1.d ####################
count.post.graduate = 69
count.female.and.post.graduate = 28
probability.female.given.post.graduate = count.female.and.post.graduate / count.post.graduate
print(paste("Probability that a randomly selected person is a female given that the
selected person is a post graduate", probability.female.given.post.graduate))
# Kind of probability is Conditional
######################################################################


############################# Question 2 #############################
yahoo.no = 268
yahoo.yes = 27
google.no = 27069
google.yes = 926
shopclues.no = 656
shopclues.yes = 29
yahoo.data = rbind(yahoo.no, yahoo.yes)
google.data =  rbind(google.no, google.yes)
shopclues.data = rbind(shopclues.no, shopclues.yes)
clear.trip.data = cbind(yahoo.data, google.data, shopclues.data)
colnames(clear.trip.data) = c('yahoo','google','shopclues')
rownames(clear.trip.data) = c('no','yes')
################### 2.a ####################
prob.clear.trip.data = data.frame(clear.trip.data / 28975)
print(prob.clear.trip.data)
################### 2.b ####################
probability.yes.and.yahoo = prob.clear.trip.data[2,1]
print(paste("Probability that the next visitor makes a purchase and comes from yahoo",
            probability.yes.and.yahoo))
# We have computed the probability of the purchase being yes and that the customer has
# arrived form yahoo.
# This is joint probability.
################### 2.c ####################
probability.google = sum(prob.clear.trip.data[,2]) / sum(prob.clear.trip.data)
print(paste("Probability that user visits from Google" , probability.google))
# I have no considered any other attributes because this is marginal probability
################### 2.d ####################
probability.yes.yahoo = probability.yes.given.yahoo / sum(prob.clear.trip.data$yahoo)
print(paste("Among visitors of yahoo probability of purchase is",probability.yes.yahoo))
######################################################################


############################# Question 3 #############################
################### 1.b ####################
yes.postive = 0.005 * 0.98
no.postive = 0.995 * 0.03
test.positive = yes.postive + no.postive
print(paste("Probability that the test gives a positive result is",test.positive))
################### 1.c ####################
probability.disease.given.yes = 0.98 * 0.005 / test.positive
print(paste("Probabilty of disease gievn that test is positive is",probability.disease.given.yes))
######################################################################

############################# Question 4 #############################
################### 4.a ####################
x.0 = 0.25
x.1 = 0.50
x.2 = 0.25
pmf.heads = data.frame(x.0,x.1,x.2)
row.names(pmf.heads) = "Probability"
colnames(pmf.heads) = c("X=0","X=1","X=2")
plot = c(0.25,0.50,0.25)
barplot(plot,xlab = "heads",ylab = "Probability")
print("Porbability Mass fucntion is ")
print(pmf.heads)
################### 4.b ####################
expectation.heads = (0*1 + 1*2 + 2*1) / 4
print(paste("Expectation of heads is",expectation.heads))
################### 4.c ####################
expectation.heads.sqaure = (0*1 + 1*2 + 4*1) / 4
variance.heads = expectation.heads.sqaure - expectation.heads^2
print(paste("Variance of heads is", variance.heads))
######################################################################


############################# Question 5 #############################
# True positive is 8000 
# False positive is 100
# False negative is 900
# True negative is 1000
precision = 8000 / (8000 + 100)
print(paste("Precision is",precision))
recall = 8000 / (8000 + 900)
print(paste("Recall is",recall))
accuracy = (8000 + 1000) / (8000 + 100 + 900 + 1000)
print(paste("Accuracy is",accuracy))
f1.score = 2*precision*recall/(precision+recall)
print(paste("F1 Score is",f1.score))
# Formulas are : 
# Accuracy  =  (total correct prediction) / (total predictions) 
# precision = true positive / total predicted positive
# recall = true positive / total actual positive 
# f1 score = 2 * (precision * recall) / (precision + recall)
# In this scenario we worry more about false positive as it might happen that we may
# give loan to a person who is not worthy and he will definetly default on his loans
######################################################################


############################# Question 6 #############################
confusion.matrix = matrix(c(80,10,1,9,120,0,5,30,100),3,3)
################### 6.a ####################
accuracy  = (80+120+100) / sum(confusion.matrix)
################### 6.b ####################
# For C :
# True Positive = 100
# True Negative  = 219
# False Positive = 35
# False Negative = 1
################### 6.c ####################
# For B:
# True positive = 120
# True negative = 186
# False positive = 9
# False negative = 40
# Recall = true positive / total actual positive
recall.B = 120 / (120+40)
print(paste("Recall for B is", recall.B))
################### 6.d ####################
# For A:
# True positive = 80
# True negative = 250
# False positive = 11
# False negative = 14
# precision = true positive / total predicted positive
precision.A = 80 / (80 + 11)
print(paste("Precision for A is", precision.A))
######################################################################


############################# Question 7 #############################
expectation.A = 80 / 100 * 4000
expectation.B = 3000
expectation.A > expectation.B
# Since the above expression is true we choose stock A as we EXPECT to gain more
# When the stock is losing:
expectation.losing.A = 80 / 100 * 4000
expectation.losing.B = 3000
expectation.losing.A > expectation.losing.B
# Since the above expression is true we EXPECT to loose more. 
# Therefore, we will switch our choice to stock B
# From the exercises we learn that we cannot take numbers at face value as they might
# be misleading. The correct apporach is to calculate the respective expectation and
# make the decision according to our business model. That's the appilcation of 
#expectation in real world
######################################################################


############################# Question 8 #############################
# We have to find A and B. As there are two unkowns we need two equations
# From the given information we know that sum of all frequiences should be 500
# This gives us the first equation that A + B = 150
# Second equation is formed using the fact that calculated expected value of 
# the sum of 20 rolls is 67.4
# this gives us the following equation : A + 6B = 525
# Now, we have to solve these two equations to get the values of A and B
coefficients = matrix(data = c(1,6,1,1), 2,2,byrow = T)
constants = matrix(data = c(525,150),2,1,byrow = T)
answer = solve(coefficients,constants)
print(paste("Value of A is",answer[1]))
print(paste("Value of B is",answer[2]))
######################################################################


############################# Question 10 ############################
dbinom(3, size = 10, prob = 0.5)
######################################################################


######################################################################